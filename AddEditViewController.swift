//
//  AddEditViewController.swift
//  GroceryImages_CoreData
//
//  Created by KVANA09 on 07/07/16.
//  Copyright © 2016 KVANA09. All rights reserved.
//

import UIKit
import CoreData

class AddEditViewController: UIViewController,NSFetchedResultsControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    var item: Item? = nil
    
    
    @IBOutlet weak var itemName: UITextField!
    
    @IBOutlet weak var itemNote: UITextField!
    
    @IBOutlet weak var itemQuantity: UITextField!
    

    @IBOutlet weak var imageHolder: UIImageView!
      let moc = (UIApplication.sharedApplication().delegate as!AppDelegate).managedObjectContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if item != nil{
        itemName.text = item?.name
        itemNote.text = item?.note
        itemQuantity.text = item?.qty
        imageHolder.image = UIImage(data: (item?.image)!)
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func saveTapped(sender: AnyObject) {
        if item != nil{
            
            editItem()
        }else{
            
            createNewItem()
        }
        
        
        
    }
    
  
    
    @IBAction func cancelTapped(sender: AnyObject) {
        
        dismissVC()
    }
    
    
    @IBAction func addImage(sender: AnyObject) {
        
        
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        pickerController.allowsEditing = true
        self.presentViewController(pickerController, animated: true, completion: nil)
    }
    
    
    @IBAction func addImageFromCamera(sender: AnyObject) {
        
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.Camera
        pickerController.allowsEditing = true
        self.presentViewController(pickerController, animated: true, completion: nil)
        
        
    }
    

    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageHolder.contentMode = .ScaleAspectFit
            self.imageHolder.image = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func dismissVC(){
        
        navigationController?.popViewControllerAnimated(true)
        
    }
    
    func createNewItem(){
        
        
        let entityDescription = NSEntityDescription.entityForName("Item", inManagedObjectContext: moc)
        
        let item = Item(entity: entityDescription!,insertIntoManagedObjectContext: moc)
        
        item.name = itemName.text
        item.note = itemNote.text
        item.qty = itemQuantity.text
        item.image = UIImagePNGRepresentation(imageHolder.image!)
        do{
           try moc.save()
            
        }catch{
            
            return
        }
    }
    
    
    func editItem(){
        
        item?.name = itemName.text
        item?.note = itemNote.text
        item?.qty = itemQuantity.text
        item?.image = UIImagePNGRepresentation(imageHolder.image!)
        
        do{
            try moc.save()
            
        }catch{
            
            return
            
        }
        
    }
        
    }
    
    


