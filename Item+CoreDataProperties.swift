//
//  Item+CoreDataProperties.swift
//  GroceryImages_CoreData
//
//  Created by KVANA09 on 07/07/16.
//  Copyright © 2016 KVANA09. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Item {

    @NSManaged var image: NSData?
    @NSManaged var name: String?
    @NSManaged var note: String?
    @NSManaged var qty: String?

}
